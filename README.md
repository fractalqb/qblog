# [qb]log
[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/qblog.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/qblog)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/qblog)](https://goreportcard.com/report/codeberg.org/fractalqb/qblog)

`import "git.fractalqb.de/fractalqb/qblog"`
---

This is a logging library that brings together

- [Slim structured
  logging](https://pkg.go.dev/git.fractalqb.de/fractalqb/sllm/v3) with

- Go's structured logging `log/slog` and

- logging configuration from
  [c4hgol](https://pkg.go.dev/git.fractalqb.de/fractalqb/c4hgol)

to make a logging library that can coexists peacefully with other logging libraries.