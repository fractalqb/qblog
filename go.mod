module git.fractalqb.de/fractalqb/qblog

go 1.22

toolchain go1.22.5

require (
	git.fractalqb.de/fractalqb/sllm/v3 v3.0.0
	github.com/fractalqb/texst v0.9.2
)
