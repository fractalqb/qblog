package qblog

import (
	"bytes"
	"context"
	"fmt"
	"log/slog"
	"strings"
	"testing"
	"time"

	"github.com/fractalqb/texst/texsting"
)

func testClock() time.Time {
	loc, _ := time.LoadLocation("Europe/Berlin")
	t := time.Date(1971, time.June, 1, 12, 13, 14, 987654321, loc)
	return t
}

const testMsg = "added `count` x `item` to shopping cart by `user`"

var testAtts = []slog.Attr{
	slog.Int("count", 7),
	slog.String("item", "Hat"),
	slog.String("user", "John Doe"),
}

func testLog(log *slog.Logger) {
	ctx := context.Background()
	log.LogAttrs(ctx, slog.LevelInfo, testMsg, testAtts...)
	log.WithGroup("test").LogAttrs(ctx, slog.LevelInfo, testMsg, testAtts...)
	log.With(slog.Int("count", 8), slog.Float64("pi", 3.1415)).LogAttrs(ctx, slog.LevelInfo, testMsg, testAtts...)
}

func TestSllmHandler(t *testing.T) {
	var buf bytes.Buffer
	// testLog(slog.New(slog.NewJSONHandler(&buf, nil)))
	// testLog(slog.New(slog.NewTextHandler(&buf, nil)))
	cfg := (&Config{}).SetWriter(&buf)
	h := NewSllmHandler(cfg)
	h.Clock = testClock
	log := slog.New(h)
	testLog(log)
	print()
	cfg.addSrc = SrcFile
	testLog(log)

	texsting.Error(t, t.Name(), &buf)
}

func TestSllmHandler_Handle_sllmError(t *testing.T) {
	var buf bytes.Buffer
	log := slog.New(NewSllmHandler((&Config{}).SetWriter(&buf)))
	log.Info("this template has `an error", `an`, "unterminated parameter")
	if s := buf.String(); !strings.Contains(s, "<sllm:unterminated parameter>") {
		t.Errorf("sllm error not detected: [%s]", s)
	}
}

func TestSllmHandler_manyAttrs(t *testing.T) {
	const n = sortRecAtts + 3
	var (
		msg, expect strings.Builder
		atts        []any
	)
	for i := 0; i < n; i++ {
		key := fmt.Sprintf("att-%d", i)
		atts = append(atts, slog.Int(key, i))
		fmt.Fprintf(&msg, " `%s`", key)
		fmt.Fprintf(&expect, " `%s:%d`", key, i)
	}
	fmt.Fprintln(&expect)
	buf := bytes.NewBuffer(make([]byte, 0, 1024))
	cfg := (&Config{}).SetWriter(buf)
	log := slog.New(NewSllmHandler(cfg))
	log.Info(msg.String(), atts...)
	output := buf.String()
	t.Log(output)
	begin := strings.Index(output, " INFO  ")
	output = output[begin+7:]
	if output != expect.String() {
		t.Errorf("output: [%s]", output)
	}
}

func TestSllmHandler_restAttrs(t *testing.T) {
	buf := bytes.NewBuffer(make([]byte, 0, 1024))
	cfg := (&Config{}).SetWriter(buf)
	log := slog.New(NewSllmHandler(cfg))
	const suffix = "{bar=true}\n"
	log.Info("`foo`", `foo`, 4711, `bar`, true)
	if msg := buf.String(); !strings.HasSuffix(msg, suffix) {
		t.Errorf("1st log without suffix: [%s]", msg)
	}
	buf.Reset()
	log.Info("`foo`", `foo`, 4711, `bar`, true)
	if msg := buf.String(); !strings.HasSuffix(msg, suffix) {
		t.Errorf("1st log without suffix: [%s]", msg)
	}
}

func BenchmarkSllmHandler(b *testing.B) {
	buf := bytes.NewBuffer(make([]byte, 0, 1024))
	cfg := (&Config{}).SetWriter(buf)
	log := slog.New(NewSllmHandler(cfg))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset()
		testLog(log)
	}
}

func BenchmarkTextHandler(b *testing.B) {
	var buf bytes.Buffer
	log := slog.New(slog.NewTextHandler(&buf, nil))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset()
		testLog(log)
	}
}

func BenchmarkJSONHandler(b *testing.B) {
	var buf bytes.Buffer
	log := slog.New(slog.NewJSONHandler(&buf, nil))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset()
		testLog(log)
	}
}

func BenchmarkSllm_attrNumber(b *testing.B) {
	buf := bytes.NewBuffer(make([]byte, 0, 1024))
	ctx := context.Background()

	cfg := (&Config{}).SetWriter(buf)
	log := slog.New(NewSllmHandler(cfg))
	// ---
	// log := slog.New(slog.NewJSONHandler(buf, nil))

	testNAtts := func(n int) func(b *testing.B) {
		return func(b *testing.B) {
			var sb strings.Builder
			var attrs []slog.Attr
			for i := 0; i < n; i++ {
				key := fmt.Sprintf("arg%d", i)
				if i > 0 {
					sb.WriteByte(' ')
				}
				fmt.Fprintf(&sb, "`%s`", key)
				attrs = append(attrs, slog.Int(key, i))
			}
			tmpl := sb.String()

			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				buf.Reset()
				log.LogAttrs(ctx, slog.LevelInfo, tmpl, attrs...)
			}
		}
	}

	b.Run("no attrs", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			buf.Reset()
			log.Info("A reasonably long message that does not have any arttributes to be processed")
		}
	})

	b.Run("1 attr", testNAtts(1))

	for n := 5; n <= 30; n += 5 {
		b.Run(fmt.Sprintf("%d attrs", n), testNAtts(n))
	}
}

// func Test_slogtest(t *testing.T) {
// 	slogtest.Run(t,
// 		func(t *testing.T) slog.Handler {

// 		}
// 	)
// }
