//go:build secret_hash

package secre

import (
	"bytes"
	"crypto/sha256"
	"encoding/ascii85"
	"fmt"
)

var (
	dstLen = ascii85.MaxEncodedLen(sha256.Size224)
)

func T(v any) string {
	var buf bytes.Buffer
	fmt.Fprint(&buf, v)
	h := sha256.Sum224(buf.Bytes())
	dst := make([]byte, dstLen)
	l := ascii85.Encode(dst, h[:])
	return string(dst[:l])
}
