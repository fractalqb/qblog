package secre

import (
	"os"
	"time"

	"git.fractalqb.de/fractalqb/qblog"
)

func testClock() time.Time {
	loc, _ := time.LoadLocation("Europe/Berlin")
	t := time.Date(1971, time.June, 1, 12, 13, 14, 987654321, loc)
	return t
}

func Example() {
	log := qblog.New((&qblog.Config{}).SetWriter(os.Stdout)).WithGroup("secreT")
	log.Handler().(*qblog.SllmHandler).Clock = testClock
	log.Info("foo `bar` baz", `bar`, T(false))
	log.Info("Just a `secret` `arg`",
		`secret`, T(4711),
		`arg`, "password",
	)
	// Output:
	// 06-01 Tu 12:13:14+01 INFO  [secreT] foo `bar:***` baz
	// 06-01 Tu 12:13:14+01 INFO  [secreT] Just a `secret:***` `arg:password`
}
