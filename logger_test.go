package qblog

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
	"unicode/utf8"
)

func Example() {
	log := New(DefaultConfig.Clone().SetWriter(os.Stdout))
	log.Handler().(*SllmHandler).Clock = testClock
	log.Info("bad login `from` at `time`", `from`, "10.0.0.1")
	log.WithGroup("tests").
		With("time", testClock()).
		Info("bad login `from` at `time`", `from`, "10.0.0.1")
	// Output:
	// 06-01 Tu 12:13:14+01 INFO  bad login `from:10.0.0.1` at `time:`
	// 06-01 Tu 12:13:14+01 INFO  [tests] bad login `from:10.0.0.1` at `time:1971-06-01 12:13:14.987654321 +0100 CET`
}

func TestLogger_source(t *testing.T) {
	var buf bytes.Buffer
	log := New((&Config{}).SetWriter(&buf).SetAddSource(SrcFile))
	log.Info("`log`", `log`, t.Name())
	_, file, line, _ := runtime.Caller(0)
	t.Log(buf.String())
	loc := fmt.Sprintf("(%s:%d)", filepath.Base(file), line-1)
	if !strings.Contains(buf.String(), loc) {
		t.Fatalf("expect location %s in [%s]", loc, buf.String())
	}
}

func TestLevel_String_equal_length(t *testing.T) {
	lvlen := utf8.RuneCountInString(LevelError.String())
	for l := LevelTrace; l < LevelError; l++ {
		ll := utf8.RuneCountInString(l.String())
		if ll != lvlen {
			t.Errorf("len(%s)==%d != %d", l.String(), ll, lvlen)
		}
	}
}
