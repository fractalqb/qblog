package qblog

import (
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"
	"sync/atomic"

	"git.fractalqb.de/fractalqb/sllm/v3"
)

type AddSource int32

const (
	SrcFile AddSource = iota + 1
	SrcPath
	SrcStack
)

type Config struct {
	writer  atomic.Value
	level   int64
	timeFmt sllm.TimeFormat
	addSrc  AddSource
}

type atomicWr struct{ w io.Writer } // atomic.Value's type must not change

var DefaultConfig Config

func init() {
	DefaultConfig.SetWriter(os.Stderr)
}

func (cfg *Config) Clone() *Config {
	clone := &Config{
		writer:  cfg.writer,
		level:   cfg.level,
		timeFmt: cfg.timeFmt,
		addSrc:  cfg.addSrc,
	}
	return clone
}

func (cfg *Config) Writer() (w io.Writer) {
	aw := cfg.writer.Load().(atomicWr)
	return aw.w
}

func (cfg *Config) SetWriter(w io.Writer) *Config {
	cfg.writer.Store(atomicWr{w})
	return cfg
}

var _ slog.Leveler = (*Config)(nil)

func (cfg *Config) Level() slog.Level {
	return slog.Level(atomic.LoadInt64(&cfg.level))
}

func (cfg *Config) SetLevel(l Level) *Config {
	atomic.StoreInt64(&cfg.level, int64(l))
	return cfg
}

func (cfg *Config) TimeFmt() sllm.TimeFormat {
	return sllm.TimeFormat(atomic.LoadInt32((*int32)(&cfg.timeFmt)))
}

func (cfg *Config) SetTimeFmt(f sllm.TimeFormat) *Config {
	atomic.StoreInt32((*int32)(&cfg.timeFmt), int32(f))
	return cfg
}

func (cfg *Config) AddSource() AddSource {
	return AddSource(atomic.LoadInt32((*int32)(&cfg.addSrc)))
}

func (cfg *Config) SetAddSource(s AddSource) *Config {
	atomic.StoreInt32((*int32)(&cfg.addSrc), int32(s))
	return cfg
}

func (cfg *Config) ParseFlag(f string) error {
	if f == "" {
		return nil
	}
	orig := f
	switch {
	case strings.HasSuffix(f, "-"):
		cfg.SetAddSource(0)
		f = f[:len(f)-1]
	case strings.HasSuffix(f, "+"):
		cfg.SetAddSource(SrcFile)
		f = f[:len(f)-1]
	case strings.HasSuffix(f, "+f"):
		cfg.SetAddSource(SrcFile)
		f = f[:len(f)-2]
	case strings.HasSuffix(f, "+p"):
		cfg.SetAddSource(SrcPath)
		f = f[:len(f)-2]
	case strings.HasSuffix(f, "+s"):
		cfg.SetAddSource(SrcStack)
		f = f[:len(f)-2]
	}
	switch f {
	case "t", "trace":
		cfg.SetLevel(LevelTrace)
	case "d", "debug":
		cfg.SetLevel(LevelDebug)
	case "i", "info":
		cfg.SetLevel(LevelInfo)
	case "w", "warn":
		cfg.SetLevel(LevelWarn)
	case "e", "error":
		cfg.SetLevel(LevelError)
	default:
		return fmt.Errorf("invalid log-config flag: '%s'", orig)
	}
	return nil
}
