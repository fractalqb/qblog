package qblog

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"log/slog"
	"path/filepath"
	"runtime"
	"slices"
	"strconv"
	"strings"
	"sync"
	"time"

	"git.fractalqb.de/fractalqb/sllm/v3"
)

type SllmHandler struct {
	cfg  *Config
	grp  string
	atts []slog.Attr

	Clock func() time.Time
}

func NewSllmHandler(cfg *Config) *SllmHandler {
	if cfg == nil {
		cfg = &DefaultConfig
	}
	return &SllmHandler{cfg: cfg}
}

func (h *SllmHandler) Enabled(_ context.Context, level slog.Level) bool {
	l := h.cfg.Level()
	return level >= slog.Level(l)
}

const sortRecAtts = 12

func (h *SllmHandler) Handle(_ context.Context, r slog.Record) (err error) {
	msg := msgSuppPool.Get().(*msgSupport)
	msg.set(h, r)

	buf := msg.buf[:0]
	if h.Clock == nil {
		buf = h.cfg.TimeFmt().Fmt(r.Time).AppendSllm(buf)
	} else {
		buf = h.cfg.TimeFmt().Fmt(h.Clock()).AppendSllm(buf)
	}
	buf = append(buf, ' ')
	buf = append(buf, Level(r.Level).String()...)
	if h.grp != "" {
		buf = append(buf, " ["...)
		buf = append(buf, h.grp...)
		buf = append(buf, ']')
	}
	if r.PC != 0 {
		switch h.cfg.addSrc {
		case 0:
		case SrcPath:
			buf = h.srcPath(buf, r.PC)
		case SrcStack:
			buf = h.srcStack(buf, r.PC)
		default:
			buf = h.srcFile(buf, r.PC)
		}
	}
	buf = append(buf, ' ')
	if r.NumAttrs() > sortRecAtts {
		slices.SortStableFunc(msg.r, attrCmpr)
		buf, err = sllm.Append(buf, r.Message, msg.argsSort)
	} else {
		buf, err = sllm.Append(buf, r.Message, msg.args)
	}
	if err != nil {
		buf = append(buf, "<sllm:"...)
		buf = append(buf, err.Error()...)
		buf = append(buf, '>')
	}
	buf = h.restAttrs(buf, msg)

	buf = append(buf, '\n')
	_, err = h.cfg.Writer().Write(buf)
	msg.buf = buf
	msgSuppPool.Put(msg)
	return err
}

func (h *SllmHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	g := h.clone()
	g.atts = append(attrs, h.atts...)
	return g
}

func (h *SllmHandler) WithGroup(name string) slog.Handler {
	if name == "" {
		return h
	}
	if strings.Contains(name, " ") {
		name = strconv.Quote(name)
	}
	g := h.clone()
	if g.grp == "" {
		g.grp = name
	} else {
		g.grp += " " + name
	}
	return g
}

func (h *SllmHandler) clone() *SllmHandler {
	clone := new(SllmHandler)
	*clone = *h
	clone.atts = slices.Clone(h.atts)
	return clone
}

func (h *SllmHandler) srcFile(buf []byte, pc uintptr) []byte {
	fs := runtime.CallersFrames([]uintptr{pc})
	f, _ := fs.Next()
	buf = append(buf, " ("...)
	buf = append(buf, filepath.Base(f.File)...)
	buf = append(buf, ':')
	buf = strconv.AppendInt(buf, int64(f.Line), 10)
	buf = append(buf, ')')
	return buf
}

func (h *SllmHandler) srcPath(buf []byte, pc uintptr) []byte {
	fs := runtime.CallersFrames([]uintptr{pc})
	f, _ := fs.Next()
	buf = append(buf, " ("...)
	buf = append(buf, f.File...)
	buf = append(buf, ':')
	buf = strconv.AppendInt(buf, int64(f.Line), 10)
	buf = append(buf, ')')
	return buf
}

func (h *SllmHandler) srcStack(buf []byte, pc uintptr) []byte {
	fs := runtime.CallersFrames([]uintptr{pc})
	f, _ := fs.Next()
	start := fmt.Sprintf("%s:%d", f.File, f.Line)

	stack := make([]byte, 1024)
	stack = stack[:runtime.Stack(stack, false)]
	scn := bufio.NewScanner(bytes.NewReader(stack))
	for scn.Scan() {
		line := scn.Text()
		if line == "" || line[0] != '\t' {
			continue
		}
		if strings.Contains(line, start) {
			break
		}
	}
	buf = append(buf, " ("...)
	buf = append(buf, filepath.Base(f.File)...)
	buf = append(buf, ':')
	buf = strconv.AppendInt(buf, int64(f.Line), 10)
	for scn.Scan() {
		line := scn.Text()
		if line == "" || line[0] != '\t' {
			continue
		}

		line = line[1:]
		sep := strings.IndexByte(line, ':')
		if sep < 0 {
			continue
		}
		file := filepath.Base(line[:sep])
		line = line[sep+1:]
		sep = 0
		for sep < len(line) && line[sep] >= '0' && line[sep] <= '9' {
			sep++
		}
		buf = append(buf, '/')
		buf = append(buf, file...)
		buf = append(buf, ':')
		buf = append(buf, line[:sep]...)
	}
	buf = append(buf, ')')
	return buf
}

func (h *SllmHandler) restAttrs(buf []byte, msg *msgSupport) []byte {
	natt := 0
	for i, a := range msg.r {
		if msg.ratts[i] {
			msg.ratts[i] = false
		} else {
			if natt == 0 {
				buf = append(buf, " {"...)
			} else {
				buf = append(buf, ' ')
			}
			buf = append(buf, a.Key...)
			buf = append(buf, '=')
			buf = sllm.AppendArg(buf, a.Value.Any())
			natt++
		}
	}
	for i, a := range h.atts {
		if msg.hatts[i] {
			msg.hatts[i] = false
		} else {
			if natt == 0 {
				buf = append(buf, " {"...)
			} else {
				buf = append(buf, ' ')
			}
			buf = append(buf, a.Key...)
			buf = append(buf, '=')
			buf = sllm.AppendArg(buf, a.Value.Any())
			natt++
		}
	}
	if natt > 0 {
		buf = append(buf, '}')
	}
	return buf
}

var msgSuppPool = sync.Pool{New: func() any {
	return &msgSupport{buf: make([]byte, 128)}
}}

type msgSupport struct {
	buf   []byte
	h     *SllmHandler
	r     []slog.Attr
	hatts []bool
	ratts []bool
}

func (supp *msgSupport) set(h *SllmHandler, r slog.Record) {
	supp.h = h
	if l := len(supp.h.atts); cap(supp.hatts) < l {
		supp.hatts = make([]bool, l)
	} else {
		supp.hatts = supp.hatts[:l]
	}

	supp.r = supp.r[:0]
	r.Attrs(func(a slog.Attr) bool { supp.r = append(supp.r, a); return true })
	if l := r.NumAttrs(); cap(supp.ratts) < l {
		supp.ratts = make([]bool, l)
	} else {
		supp.ratts = supp.ratts[:l]
	}
}

func (ha *msgSupport) args(buf []byte, idx int, name string) ([]byte, error) {
	sep := false
	for i, a := range ha.r {
		if a.Key == name {
			if sep {
				buf = append(buf, '|')
			}
			buf = sllm.AppendArg(buf, sllmValue(a.Value))
			sep = true
			ha.ratts[i] = true
		}
	}
	for i, a := range ha.h.atts {
		if a.Key == name {
			if sep {
				buf = append(buf, '|')
			}
			buf = sllm.AppendArg(buf, sllmValue(a.Value))
			sep = true
			ha.hatts[i] = true
		}
	}
	return buf, nil
}

func (ha *msgSupport) argsSort(buf []byte, _ int, name string) ([]byte, error) {
	sep := false
	idx, ok := slices.BinarySearchFunc(ha.r, slog.Attr{Key: name}, attrCmpr)
	if ok {
		a := ha.r[idx]
		buf = sllm.AppendArg(buf, sllmValue(a.Value))
		ha.ratts[idx] = true
		for idx++; idx < len(ha.r); idx++ {
			a := ha.r[idx]
			if a.Key != name {
				break
			}
			buf = append(buf, '|')
			buf = sllm.AppendArg(buf, sllmValue(a.Value))
			ha.ratts[idx] = true
		}
		sep = true
	}
	for i, a := range ha.h.atts {
		if a.Key == name {
			if sep {
				buf = append(buf, '|')
			}
			buf = sllm.AppendArg(buf, sllmValue(a.Value))
			sep = true
			ha.hatts[i] = true
		}
	}
	return buf, nil
}

func attrCmpr(a, b slog.Attr) int { return strings.Compare(a.Key, b.Key) }

func sllmValue(v slog.Value) any {
	v = v.Resolve()
	switch v.Kind() {
	case slog.KindDuration:
		return v.String()
	case slog.KindGroup:
		return v.String()
	default:
		return v.Resolve().Any()
	}
}
