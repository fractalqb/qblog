package qblog

import (
	"context"
	"fmt"
	"log/slog"
	"math"

	"git.fractalqb.de/fractalqb/sllm/v3"
)

type Level int

const (
	LevelTrace = Level(2 * slog.LevelDebug)
	LevelDebug = Level(slog.LevelDebug)
	LevelInfo  = Level(slog.LevelInfo)
	LevelWarn  = Level(slog.LevelWarn)
	LevelError = Level(slog.LevelError)
)

func (l Level) String() string {
	switch l {
	case LevelTrace:
		return "TRACE"
	case LevelDebug:
		return "DEBUG"
	case LevelInfo:
		return "INFO "
	case LevelWarn:
		return "WARN "
	case LevelError:
		return "ERROR"
	}
	p := math.Round(float64(l) / float64(LevelError))
	if p < 0 {
		return fmt.Sprintf("L%04d", int(p))
	}
	return fmt.Sprintf("L+%03d", int(p))
}

const (
	TDefault = sllm.TDefault
	TMillis  = TDefault | sllm.TMillis
	TMicros  = TDefault | sllm.TMicros
	TYear    = TDefault | sllm.TYear
	TYMillis = TMillis | sllm.TYear
	TYMicros = TMicros | sllm.TYear
)

type Logger struct{ *slog.Logger }

func New(cfg *Config) *Logger {
	return &Logger{slog.New(NewSllmHandler(cfg))}
}

func (l *Logger) Enabled(ctx context.Context, level Level) bool {
	return l.Logger.Enabled(ctx, slog.Level(level))
}

func (l *Logger) With(args ...any) *Logger {
	return &Logger{l.Logger.With(args...)}
}

func (l *Logger) WithGroup(name string) *Logger {
	return &Logger{l.Logger.WithGroup(name)}
}

func (l *Logger) Log(ctx context.Context, level Level, msg string, args ...any) {
	l.Logger.Log(ctx, slog.Level(level), msg, args...)
}

func (l *Logger) LogAttrs(ctx context.Context, level Level, msg string, attrs ...slog.Attr) {
	l.Logger.LogAttrs(ctx, slog.Level(level), msg, attrs...)
}

func (l *Logger) Trace(msg string, args ...any) {
	l.Log(context.Background(), LevelTrace, msg, args...)
}

func (l *Logger) TraceContext(ctx context.Context, msg string, args ...any) {
	l.Log(ctx, LevelTrace, msg, args...)
}

var defaultLogger = New(&DefaultConfig)

func Log(ctx context.Context, level Level, msg string, args ...any) {
	defaultLogger.Log(ctx, level, msg, args...)
}

func LogAttrs(ctx context.Context, level Level, msg string, attrs ...slog.Attr) {
	defaultLogger.LogAttrs(ctx, level, msg, attrs...)
}

func Trace(msg string, args ...any) {
	Log(context.Background(), LevelTrace, msg, args...)
}

func TraceContext(ctx context.Context, msg string, args ...any) {
	Log(ctx, LevelTrace, msg, args...)
}

func Debug(msg string, args ...any) {
	Log(context.Background(), LevelDebug, msg, args...)
}

func DebugContext(ctx context.Context, msg string, args ...any) {
	Log(ctx, LevelDebug, msg, args...)
}

func Info(msg string, args ...any) {
	Log(context.Background(), LevelInfo, msg, args...)
}

func InfoContext(ctx context.Context, msg string, args ...any) {
	Log(ctx, LevelInfo, msg, args...)
}

func Warn(msg string, args ...any) {
	Log(context.Background(), LevelWarn, msg, args...)
}

func WarnContext(ctx context.Context, msg string, args ...any) {
	Log(ctx, LevelWarn, msg, args...)
}

func Error(msg string, args ...any) {
	Log(context.Background(), LevelError, msg, args...)
}

func ErrorContext(ctx context.Context, msg string, args ...any) {
	Log(ctx, LevelError, msg, args...)
}
